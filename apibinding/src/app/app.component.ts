import { Component, OnInit } from '@angular/core';
import { PostserviceService } from './postservice.service';
import { Users } from './Users';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'apibinding';
  constructor(private rs:PostserviceService){ }
  columns = ["User Id","First Name","Last Name", "Email", "Mobile", "Salary"];

  index=["id","firstname","lastname","email","mobile","salary"];
  
  users:Users[]=[];
  ngOnInit():void{
    this.rs.getData().subscribe((response)=>{this.users=response;},)
    console.log(this.users)
   }
}
