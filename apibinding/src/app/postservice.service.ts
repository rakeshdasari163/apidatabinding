import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Users } from './Users';
@Injectable({
  providedIn: 'root'
})
export class PostserviceService {

  constructor(private http:HttpClient) { }
  url : string = "http://localhost:3000/Users";
  getData(){
    return this.http.get<Users[]>(this.url);
 }
   

}
